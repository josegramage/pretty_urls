
    
<p>Introduzca sus datos para registrarse:</p>

<form name="form" class="form" id="form">
    <span id="e_errors" class="styerror"></span>
  
    <table width="50%"  border="0" cellspacing="0" cellpadding="0">
        <tr>
            <td>Nombre</td>
            <td><input name="nombre" type="text" class="nombre" id="nombre" placeholder=" Introduzca su nombre" value="">
                <span class="e_nombre" class="styerror"></span>     
            </td>
        </tr>

        <tr>
            <td>Apellidos</td>
            <td><input name="apellidos" type="text" class="apellidos" id="apellidos" placeholder=" Introduzca sus apellidos" value="">
                <span id="e_apellidos" class="styerror"></span>       
            </td>
        </tr>

        <tr>
            <td>Email</td>
            <td><input name="email" type="text" class="email" id="email" placeholder=" Introduzca su email" value="" >
                <span id="e_email" class="styerror"></span>  
            </td>
        </tr>

        <tr>
            <td>Fecha de nacimiento</td>
            <td><input name="date_birthday" type="text" class="date_birthday" id="date_birthday" placeholder=" Introduzca su fecha de nacimiento" value="" >
                <span id="e_date_birthday" class="styerror"></span>          
            </td>

        <tr>
            <td>Fecha de alta</td>
            <td><input name="fecha_alta" type="text" id="fecha_alta" class="fecha_alta" placeholder=" Introduzca su fecha de alta" value="" >
                <span id="e_fecha_alta" class="styerror"></span>      
            </td>
        </tr>

        <tr>
            <td>Usuario</td>
            <td><input name="usuario" type="text" id="usuario" class="usuario" placeholder=" Introduzca el nombre de usuario" value="" >
                <span id="e_usuario" class="styerror"></span>           
            </td>
        </tr>

        <tr>
            <td>Password</td>
            <td><input name="password" type="password" placeholder="Introduzca su password" class="password" id="password" value="">
                <span id="e_password" class="styerror"></span>             
            </td>
        </tr>

        <tr>
            <td>Password2</td>
            <td><input name="password2" type="password" placeholder="Repita su password" class="password2" id="password2" value="">
                <span id="e_password2" class="styerror"></span>

            </td>
        </tr>
        <tr>
    <!--        <td>Nivel de ingles </td>
       <td>Ninguno <input name="nivel" type="radio" value="ninguno" checked="">
                A1 <input name="nivel" type="radio" value="A1">
                A2 <input name="nivel" type="radio" value="A2">
                B1 <input name="nivel" type="radio" value="B1">
                B2 <input name="nivel" type="radio" value="B2">
                C1 <input name="nivel" type="radio" value="C1">
                C2 <input name="nivel" type="radio"></td>
        </tr> -->

        <tr>
            <td>Estudios:</td>
            <td>Ninguno <input type="checkbox" class="messageCheckbox" name="estudios[]" value="ninguno" checked="">
                Bachiller <input type="checkbox" class="messageCheckbox" name="estudios[]" value="bachiller">
                Grado Medio  <input type="checkbox" class="messageCheckbox" name="estudios[]" value="grado_medio">
                Grado Superior <input type="checkbox" class="messageCheckbox" name="estudios[]" value="grado_superior">
                Universidad  <input type="checkbox" class="messageCheckbox" name="estudios[]" value="u
                                    niversidad">  
                <span id="e_estudios" class="styerror"></span>
            </td>
        </tr>

        <tr>
            <td>
    <!-- /*        <label for="avatar">Avatar</label>
                <input name="MAX_FILE_SIZE" type="hidden" value="5120000" />
                <label for="avatar" class="custom-file-upload">Choose file</label>
                <input id="avatar" name="avatar" type="file" readonly="readonly" />
                <span id="e_avatar" class="styerror"></span>

                <img id="img_avatar" width="75" height="75"  src="media/default-avatar.png" /><br><br>
                <div id="progress">
                    <div id="bar"></div>
                    <div id="percent">0%</div >
                </div>

                <div class="msg"></div>
                <br/>
           */   -->
                
                <p>
    		<span id="e_avatar" class="styerror"></span>
		</p>
		
		<div id="progress">
			<div id="bar"></div>
			<div id="percent"></div >
		</div>
		
		<div class="msg"></div><br/>
		
		<div id="dropzone" class="dropzone"></div><br/>
                         
            </td>
        </tr> 

        <td><input type="button" name="SubmitUsers" class="SubmitUsers" value="Enviar" id="SubmitUsers"></td>
    </table>
</form>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css">
 <script type="text/javascript" src="modules/users/view/js/users.js" ></script>   

