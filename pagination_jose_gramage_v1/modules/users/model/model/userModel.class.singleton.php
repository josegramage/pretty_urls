<?php 
	$path=$_SERVER['DOCUMENT_ROOT'].'/DB_jose_gramage_v1/';
    define('SITE_ROOT', $path);
    require(SITE_ROOT . "modules/users/model/BLL/userBLL.class.singleton.php");
  
	class userModel {
		private $bll;
        static $_instance;

        private function __construct() {
            $this->bll = userBLL::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
    
        public function create_user($arrArgument) {
            return $this->bll->create_user_BLL($arrArgument);
        }
	}
