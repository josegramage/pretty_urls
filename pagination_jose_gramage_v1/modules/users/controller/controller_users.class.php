<?php

session_start();
include("../utils/functions_users.inc.php");
include("../../../utils/upload.php");


if ((isset($_GET["upload"])) && ($_GET["upload"] == true)) {
    $result_avatar = upload_files();
    $_SESSION['result_avatar'] = $result_avatar;
    //echo debug($_SESSION['result_avatar']); //se mostraría en alert(response); de dropzone.js
}

if (isset($_POST['alta_users_json'])) {
    alta_users_json();
}

function alta_users_json() {
    $jsondata = array();

    $usersJSON = json_decode($_POST["alta_users_json"], true);
    //    echo debug($usersJSON);

    $result = validate_user($usersJSON);
    //	echo debug($result); //se mostraría en alert(response); de $.post('pages/controller_users.pp ...

    if (empty($_SESSION['result_avatar'])) {
        $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => './media/default-avatar.png');
    }
    //echo debug($_SESSION['result_avatar']); //se mostraría en alert(response); de $.post('pages/controller_users.pp ...

    $result_avatar = $_SESSION['result_avatar'];
    //$_SESSION['result_avatar'] = array(); ///// 
    //echo debug($result);
    //echo debug($result_avatar);

    if (($result['resultado']) && ($result_avatar['resultado'])) {

        $arrArgument = array(
            'nombre' => $result['datos']['nombre'],
            'apellidos' => $result['datos']['apellidos'],
            'email' => $result['datos']['email'],
            'date_birthday' => $result['datos']['date_birthday'],
            'fecha_alta' => $result['datos']['fecha_alta'],
            'usuario' => $result['datos']['usuario'],
            'password' => $result['datos']['password'],
            'estudios' => $result['datos']['estudios'],
            'avatar' => $result_avatar['datos']
        );

        $_SESSION['result_avatar'] = array();
        
        
   ///////////////// insert into BD begin ////////////////////////
			
			$arrValue = false;
			$path_model = $_SERVER['DOCUMENT_ROOT'] . '/list_user_jose_gramage_v1/modules/users/model/model/';
			$arrValue = loadModel($path_model, "userModel", "create_user", $arrArgument);
			
			if($arrValue)
				$mensaje="Su registro se ha efectuado correctamente, para finalizar compruebe que ha recibido un correo de validacion y siga sus instrucciones";
			else
				$mensaje="No se ha podido realizar su alta. Intentelo mas tarde";
       

        
   /* 
       /////////////////insert into BD////////////////////////
		//	$host = '51.254.113.182';
                        $host = '127.0.0.1';
			$usuarioBD = "root";
                	$passwordBD = "admin1234";
			$db = "rural_shop";
	
			$conexion = mysql_connect($host, $usuarioBD, $passwordBD) or die("No se pudo conectar al host");
			mysql_select_db($db, $conexion) or die("No se pudo conectar a la base de datos");
			
			
			$nombre = $arrArgument['nombre'];
			$apellidos = $arrArgument['apellidos'];
			$email = $arrArgument['email'];
                        $date_birthday = $arrArgument['date_birthday'];
                        $fecha_alta = $arrArgument['fecha_alta'];
                        $usuario = $arrArgument['usuario'];
			$password = $arrArgument['password'];
		//	$password2 = $arrArgument['password2'];
                        $estudios = $arrArgument['estudios'];
			$avatar = $arrArgument['avatar'];
			
			$sql = "INSERT INTO users (nombre, apellidos, email, date_birthday, fecha_alta, usuario,  "
                . " password, estudios, avatar"
                . " ) VALUES ('$nombre', '$apellidos', '$email', '$date_birthday','$fecha_alta',"
                . " '$usuario', '$password', '$estudios', '$avatar')";
			$resultado = mysql_query($sql);
			
			if($resultado)
				$mensaje="Su registro se ha efectuado correctamente, para finalizar compruebe que ha recibido un correo de validacion y siga sus instrucciones";
			else
				$mensaje="No se ha podido realizar su alta. Intentelo mas tarde";
        
        */
        /////////////////////////////////////////////////////////
        
        $_SESSION['user'] = $arrArgument;
        $_SESSION['msje'] = $mensaje;
        $url = "index.php?module=users&view=results_users";

        $jsondata["success"] = true;
        $jsondata["redirect"] = $url;
        echo json_encode($jsondata);
        exit;
    } else {
        $jsondata["success"] = false;
        $jsondata["error"] = $result['error'];
        $jsondata["error_avatar"] = $result_avatar['error'];

        $jsondata["success1"] = false;
        if ($result_avatar['resultado']) {
            $jsondata["success1"] = true;
            $jsondata["img_avatar"] = $result_avatar['datos'];
        }

         header('HTTP/1.0 404 Not Found', true, 404);
        echo json_encode($jsondata);
        //    exit;
    }
}

if (isset($_GET["delete"]) && $_GET["delete"] == true) {
    $_SESSION['result_avatar'] = array();
    $result = remove_files();
    if ($result === true) {
        echo json_encode(array("res" => true));
    } else {
        echo json_encode(array("res" => false));
    }
}

//////////////////////////////////////////////////////////////// load
if (isset($_GET["load"]) && $_GET["load"] == true) {
    $jsondata = array();
    if (isset($_SESSION['user'])) {
        //echo debug($_SESSION['user']);
        $jsondata["user"] = $_SESSION['user'];
    }
    if (isset($_SESSION['msje'])) {
        //echo $_SESSION['msje'];
        $jsondata["msje"] = $_SESSION['msje'];
    }
    close_session();
    echo json_encode($jsondata);
    exit;
}

function close_session() {
    unset($_SESSION['user']);
    unset($_SESSION['msje']);
    $_SESSION = array(); // Destruye todas las variables de la sesión
    session_destroy(); // Destruye la sesión
}
///////////////////////// 

if ((isset($_GET["load_data"])) && ($_GET["load_data"] == true)) {
    $jsondata = array();

    if (isset($_SESSION['user'])) {
        $jsondata["user"] = $_SESSION['user'];
        echo json_encode($jsondata);
        exit;
    } else {
       $jsondata["user"] = "";
        echo json_encode($jsondata);
        exit;
    }
}