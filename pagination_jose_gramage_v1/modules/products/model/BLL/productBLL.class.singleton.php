<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/pagination_jose_gramage_v1/';
define('SITE_ROOT', $path);
define('MODEL_PATH', SITE_ROOT . 'model/');

require (MODEL_PATH . "Db.class.singleton.php");
require(SITE_ROOT . "modules/products/model/DAO/productDAO.class.singleton.php");




class productBLL {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = productDAO::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    function list_products_BLL() {
     //   $product = new activities_dao();
        return $this->dao->list_products_DAO($this->db);
    }

    function details_products_BLL($arrArgument) {
    //    $product = new activities_dao();
        return $this->dao->details_products_DAO($this->db, $arrArgument);
    }


}
