<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/pagination_jose_gramage_v1/';
define('SITE_ROOT', $path);
require(SITE_ROOT . "modules/products/model/BLL/productBLL.class.singleton.php");


class productModel {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = productBLL::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    function list_products() {
        return $this->bll->list_products_BLL();
    }

    function details_products($arrArgument) {    
        return $this->bll->details_products_BLL($arrArgument);
    }
}