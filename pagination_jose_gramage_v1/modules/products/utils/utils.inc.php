<?php

    function paint_template_error($message){
		print ("<section> \n");
	      		print ("<div class='container'> \n");
	      		print ("<div class='row text-center pad-row'> \n");
	      		
	      			print ("<h1>ERROR 404 - ". $message ."</h1> \n");
	        		print ("<br><br><br><br> \n");
	        	
	        		print ("<h2>The following error occurred:</h2> \n");
	        		print ("<p>The requested URL was not found on this server.</p> \n");
	        		print ("<P>Please check the URL or contact the <!--WEBMASTER//-->webmaster<!--WEBMASTER//-->.</p> \n");
	        
	            	print ("<br><br><br><br> \n");
	            	print ("<p>Powered by <a href='http://www.ispconfig.org'>ISPConfig</a></p> \n");
				
				print ("</div> \n");
				print ("</div> \n");
			print ("</section> \n");
	}
	
	function paint_template_products($resultado){
		print ("<section> \n");
      		print ("<div class='container'> \n");
      		print ("<div class='row text-center pad-row'> \n");
        	while ($productos = mysqli_fetch_array($resultado)) {
    			print ("<div class='col-md-4  col-sm-4'> \n");
	            	print ("<img src='view/img/product.jpg' alt='product' height='70' width='70'> \n");
	            	//print ("<img src='". $productos['img'] . "' alt='product' height='70' width='70'> \n");
	
	            	print ("<h4> <strong>" . $productos['nombre'] . "</strong> </h4> \n");
	            	print ("<p>" . $productos['descripcion'] . "</p> \n");
	                print ("<h5> <strong>Precio: " . $productos['precio'] . "€</strong> </h5> \n");
	                
	               	$callback="index.php?module=services&idProducto=".$productos['id'];
	               	print ("<a href='". $callback ."' class='btn btn-primary' >Read Details</a> \n");
	            
	            	print ("<br><br><br><br> \n");
        		print ("</div> \n");
        	}
			print ("</div> \n");
			print ("</div> \n");
		print ("</section> \n");
	}