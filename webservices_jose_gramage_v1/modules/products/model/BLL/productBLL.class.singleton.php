<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/webservices_jose_gramage_v1/';
//define('SITE_ROOT', $path);
//define('MODEL_PATH', SITE_ROOT . 'model/');

require ($path . "model/Db.class.singleton.php");
require($path . "modules/products/model/DAO/productDAO.class.singleton.php");

class productBLL {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = productDAO::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    function list_products_BLL() {
        return $this->dao->list_products_DAO($this->db);
    }

    function details_products_BLL($id) {
        return $this->dao->details_products_DAO($this->db, $id);
    }

    function n_pages_BLL() {
        return $this->dao->n_pages_DAO($this->db);
    }

    function total_pages_BLL($arrArgument) {
        return $this->dao->total_pages_DAO($this->db, $arrArgument);
    }

    function order_by_name_BLL() {
        return $this->dao->order_by_name_DAO($this->db);
    }

    function distinct_like_BLL($criteria) {
        return $this->dao->distinct_like_DAO($this->db, $criteria);
    }

    function count_like_BLL($criteria) {
        return $this->dao->count_like_DAO($this->db, $criteria);
    }
    
    function total_products_BLL($criteria) {
        return $this->dao->total_products_DAO($this->db, $criteria);
    }

}
