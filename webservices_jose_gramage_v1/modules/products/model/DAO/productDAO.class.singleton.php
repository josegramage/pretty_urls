<?php

class productDAO {

    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_products_DAO($db) {
        $sql = "SELECT * FROM tienda"; // LIMIT $from , $perPage";   
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function details_products_DAO($db, $id) {
        $sql = "SELECT * FROM tienda WHERE id = '$id'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function n_pages_DAO($db) {
        $sql = "SELECT COUNT(*) as total FROM tienda";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function total_pages_DAO($db, $arrArgument) {
        $position = $arrArgument["position"];
        $item_per_page = $arrArgument["item_per_page"];
        $criteria = $arrArgument["criteria"];

        $sql = "SELECT DISTINCT * FROM tienda WHERE nombre like '%" . $criteria . "%' ORDER BY id ASC LIMIT $position, $item_per_page";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function order_by_name_DAO($db) {
        $sql = "SELECT nombre FROM tienda ORDER BY nombre";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function distinct_like_DAO($db, $criteria) {
        $sql = "SELECT DISTINCT * FROM tienda WHERE nombre like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

    public function count_like_DAO($db, $criteria) {
        $sql = "SELECT COUNT(*) as total FROM tienda WHERE nombre like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }
    
       public function total_products_DAO($db, $criteria) {
        $sql = "SELECT COUNT(*) as total FROM tienda WHERE nombre like '%" . $criteria . "%'";
        $stmt = $db->ejecutar($sql);
        return $db->listar($stmt);
    }

}