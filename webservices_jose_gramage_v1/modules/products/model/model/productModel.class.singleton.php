<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/webservices_jose_gramage_v1/';
//define('SITE_ROOT', $path);
require($path . "modules/products/model/BLL/productBLL.class.singleton.php");


class productModel {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = productBLL::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    function list_products() {
        return $this->bll->list_products_BLL();
    }

    function details_products($id) {    
        return $this->bll->details_products_BLL($id);
    }
    
    function n_pages() {
        return $this->bll->n_pages_BLL();
    }

    function total_pages($arrArgument) {
        return $this->bll->total_pages_BLL($arrArgument);
    }
    
    function order_by_name() {
        return $this->bll->order_by_name_BLL();
    }

    function distinct_like($criteria) {
        return $this->bll->distinct_like_BLL($criteria);
    }

    function count_like($criteria) {
        return $this->bll->count_like_BLL($criteria);
    }
    
    function total_products($criteria) {
        return $this->bll->total_products_BLL($criteria);
    }
}