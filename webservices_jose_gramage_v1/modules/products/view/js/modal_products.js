$(document).ready(function () {
    $('.nombre_producto').click(function () {
        var id = this.getAttribute('id');
        //alert(id);

        $.get("modules/products/controller/controller_products.class.php?idProducto=" + id, function (data, status) {
            var json = JSON.parse(data);
            var product = json.product;
             var datos = product[0];
            //alert(product.nombre);
            //console.log(product);

            $('#results').html('');
            $('.pagination').html('');

            $('#img_product').html('<img src="' + datos.img + '" height="75" width="75"> ');
            $('#nom_product').html(datos.nombre);
            $('#desc_product').html(datos.descripcion);
            $('#price_product').html("Precio: " + datos.precio + " €");
            $('#back_list').html('<a href="http://51.254.113.182/webservices_jose_gramage_v1/index.php?module=products&view=list_products">Back To List Products</a>');
            


            /*$("#product").dialog({ 
             width: 690,  <!-- -------------> ancho de la ventana -->
             height: 450,<!--  -------------> altura de la ventana -->
             //show: "scale", <!-- -----------> animación de la ventana al aparecer -->
             //hide: "scale", <!-- -----------> animación al cerrar la ventana -->
             resizable: "false", <!-- ------> fija o redimensionable si ponemos este valor a "true" -->
             //position: "down",<!--  ------> posicion de la ventana en la pantalla (left, top, right...) -->
             modal: "true", <!-- ------------> si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
             buttons: {
             Ok: function() {
             $( this ).dialog( "close" );
             }
             },
             show: {
             effect: "blind",
             duration: 1000
             },
             hide: {
             effect: "explode",
             duration: 1000
             }	
             });*/
        })
                .fail(function (xhr) {
                    if (xhr.status === 404) {
                        $("#results").load("modules/products/controller/controller_products.class.php?view_error=false"); //view_error=false
                    } else {
                        $("#results").load("modules/products/controller/controller_products.class.php?view_error=true");
                    }
                    $('.pagination').html('');

                    $('#img_product').html('');
                    $('#nom_product').html('');
                    $('#desc_product').html('');
                    $('#price_product').html('');

                    $('#keyword').val('');
                });
    });
}); 