<?php

$path = $_SERVER['DOCUMENT_ROOT'] . '/webservices_jose_gramage_v1/';
define('SITE_ROOT', $path);

include $path . "modules/products/utils/utils.inc.php";
include $path . "utils/common.inc.php";
include $path . 'paths.php';
include $path . 'classes/Log.class.php';
include $path . "utils/filters.inc.php";
include $path . 'utils/response_code.inc.php';

$_SESSION['module'] = "products";

if ((isset($_GET["autocomplete"])) && ($_GET["autocomplete"] === "true")) {

    set_error_handler('ErrorHandler');
    try {
        $path_model = SITE_ROOT . '/modules/products/model/model/';
        $nom_productos = loadModel($path_model, "productModel", "order_by_name");
        //throw new Exception(); //que entre en el catch
    } catch (Exception $e) {
        showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
    }
    restore_error_handler();

    if ($nom_productos) {
        $jsondata["nom_productos"] = $nom_productos;
        echo json_encode($jsondata);
        exit;
    } else {
        //if($nom_productos){ //que lance error si no hay productos
        showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
    }
}
if (($_GET["nom_product"])) {
    //filtrar $_GET["nom_product"]
    $result = filter_string($_GET["nom_product"]);
    if ($result['resultado']) {
        $criteria = $result['datos'];
    } else {
        $criteria = '';
    }

    set_error_handler('ErrorHandler');
    try {

        $path_model = SITE_ROOT . '/modules/products/model/model/';
        $product = loadModel($path_model, "productModel", "distinct_like", $criteria);
        //throw new Exception(); //que entre en el catch
    } catch (Exception $e) {
        showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
    }
    restore_error_handler();

    if ($product) {
        $jsondata["product_autocomplete"] = $product[0];
        echo json_encode($jsondata);
        exit;
    } else {
        //if($producto){{ //que lance error si no existe el producto
        showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
    }
}


if (($_GET["count_product"])) {
    
       
    //filtrar $_GET["count_product"]
    $result = filter_string($_GET["count_product"]);
    if ($result['resultado']) {
        $criteria = $result['datos'];
    } else {
        $criteria = '';
    }

   // set_error_handler('ErrorHandler');
    try {
     //   throw new Exception();
        $path_model = SITE_ROOT . '/modules/products/model/model/';
        $resultado = loadModel($path_model, "productModel", "count_like", $criteria);
        $total_rows = $resultado;
    } catch (Exception $e) {
        showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
    }
    restore_error_handler();

    if ($total_rows) {
        $jsondata["num_products"] = $total_rows[0];
        echo json_encode($jsondata);
        exit;
    } else {
        //if($total_rows){ //que lance error si no existe el producto
        showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
    }
}


if ((isset($_GET["num_pages"])) && ($_GET["num_pages"] == true)) {

    if (isset($_GET["keyword"])) {
        $result = filter_string($_GET["keyword"]);
        if ($result['resultado']) {
            $criteria = $result['datos'];
        } else {
            $criteria = '';
        }
    } else {
        $criteria = '';
    }

    set_error_handler('ErrorHandler');
    $item_per_page = 6;
    $pages= 0;
    try {
        $path_model = $path . '/modules/products/model/model/';
        $arrValue = loadModel($path_model, "productModel", "total_products", $criteria);
        $get_total_rows = $arrValue[0]["total"];
        $pages = ceil($get_total_rows / $item_per_page); //break total records into pages
    } catch (Exception $e) {
        showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
    }
    restore_error_handler();

    if ($get_total_rows) {
        $jsondata["pages"] = $pages;
        echo json_encode($jsondata);
        exit;
    } else {
        showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
    }
}

if ((isset($_GET["view_error"])) && ($_GET["view_error"] == "true")) {
    showErrorPage(0, "ERROR - 503 BD Unavailable");
}
if ((isset($_GET["view_error"])) && ($_GET["view_error"] == "false")) {
    //showErrorPage(0, "ERROR - 404 NO PRODUCTS");
    showErrorPage(3, "RESULTS NOT FOUND");
}

if ($_GET["idProducto"]) {
  //  $arrValue = null;
    $result = filter_num_int($_GET["idProducto"]);
    if ($result['resultado']) {
        $id = $result['datos'];
    } else {
        $id = 1;
    }

    set_error_handler('ErrorHandler');
    try {
        $path_model = SITE_ROOT . '/modules/products/model/model/';
        $arrValue = loadModel($path_model, "productModel", "details_products", $id);
    } catch (Exception $e) {
        showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
    }
    restore_error_handler();


    if ($arrValue) {
        $jsondata["product"] = $arrValue;
    //    $jsondata["product"] = $arrValue[0];
        echo json_encode($jsondata);
        exit;
    } else {
        showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
    }
} else {

    if (isset($_POST["page_num"])) {
        $result = filter_num_int($_POST["page_num"]);
        if ($result['resultado']) {
            $page_number = $result['datos'];
        }
    } else {
        $page_number = 1;
    }

    if (isset($_GET["keyword"])) {
        $result = filter_string($_GET["keyword"]);
        if ($result['resultado']) {
            $criteria = $result['datos'];
        } else {
            $criteria = '';
        }
    } else {
        $criteria = '';
    }


    if (isset($_POST["keyword"])) {
        $result = filter_string($_GET["keyword"]);
        if ($result['resultado']) {
            $criteria1 = $result['datos'];
        } else {
            $criteria1 = '';
        }
    } else {
        $criteria1 = '';
    }

    if (isset($_POST["keyword"])) {
        $criteria = $criteria1;
    }

    set_error_handler('ErrorHandler');
    try {

        $item_per_page = 6;

        $position = (($page_number - 1) * $item_per_page);
        $arrArgument = array(
            'position' => $position,
            'item_per_page' => $item_per_page,
            'criteria' => $criteria
        );
        $path_model = SITE_ROOT . '/modules/products/model/model/';
        $arrValue = loadModel($path_model, "productModel", "total_pages", $arrArgument);
    } catch (Exception $e) {
        showErrorPage(0, "ERROR - 503 BD Unavailable");
    }
    restore_error_handler();

    if ($arrValue) {
        paint_template_products($arrValue);
    } else {
        showErrorPage(0, "ERROR - 404 NO PRODUCTS");
    }
}