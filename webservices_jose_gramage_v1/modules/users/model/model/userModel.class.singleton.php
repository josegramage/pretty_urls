<?php 
	$path=$_SERVER['DOCUMENT_ROOT'].'/webservices_jose_gramage_v1/';
    define('SITE_ROOT', $path);
    require(SITE_ROOT . "modules/users/model/BLL/userBLL.class.singleton.php");
  
	class userModel {
	private $bll;
        static $_instance;

        private function __construct() {
            $this->bll = userBLL::getInstance();
        }

        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
    
        public function create_user($arrArgument) {
            return $this->bll->create_user_BLL($arrArgument);
        }
        
         public function obtain_paises($url) {
            return $this->bll->obtain_paises_BLL($url);
        }
        
        public function obtain_provincias() {
            return $this->bll->obtain_provincias_BLL();
        }
        
        public function obtain_poblaciones($arrArgument) {
            return $this->bll->obtain_poblaciones_BLL($arrArgument);
        }
        
	}
