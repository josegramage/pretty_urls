<?php
    class userDAO {
        static $_instance;
        private function __construct() {     
        }
        public static function getInstance() {
            if (!(self::$_instance instanceof self))
                self::$_instance = new self();
            return self::$_instance;
        }
    
        public function create_user_DAO($db, $arrArgument) {
            $usuario = $arrArgument['usuario'];
			$nombre = $arrArgument['nombre'];
			$apellidos = $arrArgument['apellidos'];
			$email = $arrArgument['email'];
                        $date_birthday = $arrArgument['date_birthday'];
                        $fecha_alta = $arrArgument['fecha_alta'];
                        $usuario = $arrArgument['usuario'];
			$password = $arrArgument['password'];
		        $estudios = $arrArgument['estudios'];
			$avatar = $arrArgument['avatar'];
                        $pais = $arrArgument['pais'];
			$provincia = $arrArgument['provincia'];
			$poblacion = $arrArgument['poblacion'];
			
                        $ninguno=0;
                        $bachiller=0;
                        $grado_medio=0;
                        $grado_superior=0;
                        $universidad=0;
              
                        foreach ($estudios as $estudiosDB){    
                            if ($estudiosDB === "ninguno")
                                $ninguno =1;
                            if ($estudiosDB === "bachiller")
                                $bachiller =1;
                            if ($estudiosDB === "grado_medio")
                                $grado_medio =1;
                            if ($estudiosDB === "grado_superior")
                                $grado_superior =1;
                            if ($estudiosDB === "universidad")
                                $universidad =1;
                        }
        
			$sql = "INSERT INTO users (nombre, apellidos, email, date_birthday, fecha_alta, usuario,  "
                . " password, ninguno, bachiller, grado_medio, grado_superior, universidad, pais, provincia, poblacion, avatar"
                . " ) VALUES ('$nombre', '$apellidos', '$email', '$date_birthday','$fecha_alta',"
                . " '$usuario', '$password', '$ninguno', '$bachiller', '$grado_medio', '$grado_superior', '$universidad','$pais','$provincia','$poblacion', '$avatar')";
            
            return $db->ejecutar($sql);
        }
        
        public function obtain_paises_DAO($url) {
            $ch = curl_init();
            curl_setopt ($ch, CURLOPT_URL, $url);
            curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
            $file_contents = curl_exec($ch);
            curl_close($ch);
            
            return ($file_contents) ? $file_contents : FALSE;
        }
        
        public function obtain_provincias_DAO() {
            $json = array();
		    $tmp = array();

    		$provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT']. "/webservices_jose_gramage_v1/resources/provinciasypoblaciones.xml");
    		$result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
    		for ($i=0; $i<count($result); $i+=2) {
    			$e=$i+1;
    			$provincia=$result[$e];
    				
    			$tmp = array(
    				'id' => (string) $result[$i], 'nombre' => (string) $provincia	
    			);
    			array_push($json, $tmp);
    		}
            return $json;
        }
        
        public function obtain_poblaciones_DAO($arrArgument) {
            $json = array();
		    $tmp = array();
        
            $filter = (string)$arrArgument;
    	    $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT']. "/webservices_jose_gramage_v1/resources/provinciasypoblaciones.xml");
		    $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");
		
        	for ($i=0; $i<count($result[0]); $i++) {
        		$tmp = array(
        			'poblacion' => (string) $result[0]->localidad[$i]	
        		);
        		array_push($json, $tmp);
        	}
            return $json;
        }
        
    }