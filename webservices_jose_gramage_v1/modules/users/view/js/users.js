jQuery.fn.LlenarLimpiarCampos = function () {
    this.each(function () {

        if ($(".nombre").attr("value") == "") {
            $(".nombre").attr("value", "Introduzca su nombre");
            $(".nombre").focus(function () {
                if ($(".nombre").attr("value") == "Introduzca su nombre") {
                    $(".nombre").attr("value", "");
                }
            });
        }
        $(".nombre").blur(function () {
            if ($(".nombre").attr("value") === "") {
                $(".nombre").attr("value", "Introduzca su nombre");
            }
        });

        if ($(".apellidos").attr("value") === "") {
            $(".apellidos").attr("value", "Introduzca sus apellidos");
            $(".apellidos").focus(function () {
                if ($(".apellidos").attr("value") === "Introduzca sus apellidos") {
                    $(".apellidos").attr("value", "");
                }
            });
        }
        $(".apellidos").blur(function () {
            if ($(".apellidos").attr("value") === "") {
                $(".apellidos").attr("value", "Introduzca sus apellidos");
            }
        });

        if ($(".email").attr("value") === "") {
            $(".email").attr("value", "Introduzca su email");
            $(".email").focus(function () {
                if ($(".email").attr("value") === "Introduzca su email") {
                    $(".email").attr("value", "");
                }
            });
        }
        $(".email").blur(function () {
            if ($(".email").attr("value") === "") {
                $(".email").attr("value", "Introduzca su email");
            }
        });

        if ($(".date_birthday").attr("value") === "") {
            $(".date_birthday").attr("value", "Introduzca su fecha de nacimiento");
            $(".date_birthday").focus(function () {
                if ($(".date_birthday").attr("value") === "Introduzca su fecha de nacimiento") {
                    $(".date_birthday").attr("value", "");
                }
            });
        }
        $(".date_birthday").blur(function () {
            if ($(".date_birthday").attr("value") === "") {
                $(".date_birthday").attr("value", "Introduzca su fecha de nacimiento");
            }
        });

        if ($(".fecha_alta").attr("value") === "") {
            $(".fecha_alta").attr("value", "Introduzca su fecha de alta");
            $(".fecha_alta").focus(function () {
                if ($(".fecha_alta").attr("value") === "Introduzca su fecha de alta") {
                    $(".fecha_alta").attr("value", "");
                }
            });
        }
        $(".fecha_alta").blur(function () {
            if ($(".fecha_alta").attr("value") === "") {
                $(".fecha_alta").attr("value", "Introduzca su fecha de alta");
            }
        });

        if ($(".usuario").attr("value") === "") {
            $(".usuario").attr("value", "Introduzca su usuario");
            $(".usuario").focus(function () {
                if ($(".usuario").attr("value") === "Introduzca su usuario") {
                    $(".usuario").attr("value", "");
                }
            });
        }
        $(".usuario").blur(function () {
            if ($(".usuario").attr("value") === "") {
                $(".usuario").attr("value", "Introduzca su usuario");
            }
        });

        if ($(".password").attr("value") === "") {
            $(".password").attr("value", "");
            $(".password").focus(function () {
                if ($(".password").attr("value") === "") {
                    $(".password").attr("value", "");
                }
            });
        }
        $(".password").blur(function () {
            if ($(".password").attr("value") === "") {
                $(".password").attr("value", "Introduzca su password");
            }
        });

        if ($(".password2").attr("value") === "") {
            $(".password2").attr("value", "");
            $(".password2").focus(function () {
                if ($(".password2").attr("value") === "") {
                    $(".password2").attr("value", "");
                }
            });
        }
        $(".password2").blur(function () {
            if ($(".password2").attr("value") === "") {
                $(".password2").attr("value", "Introduzca su password");
            }
        });
    });
    return this;
};


function validate_pais(pais) {
    if (pais == null) {
        //return 'default_pais';
        return false;
    }
    if (pais.length == 0) {
        //return 'default_pais';
        return false;
    }
    if (pais === 'Selecciona un Pais') {
        //return 'default_pais';
        return false;
    }
    if (pais.length > 0) {
        var regexp = /^[a-zA-Z]*$/;
        return regexp.test(pais);
    }
    return false;
}
function validate_provincia(provincia) {
    if (provincia == null) {
        return 'default_provincia';
    }
    if (provincia.length == 0) {
        return 'default_provincia';
    }
    if (provincia === 'Selecciona una Provincia') {
        //return 'default_provincia';
        return false;
    }
    if (provincia.length > 0) {
        var regexp = /^[a-zA-Z0-9, ]*$/;
        return regexp.test(provincia);
    }
    return false;
}
function validate_poblacion(poblacion) {
    if (poblacion == null) {
        return 'default_poblacion';
    }
    if (poblacion.length == 0) {
        return 'default_poblacion';
    }
    if (poblacion === 'Selecciona una Poblacion') {
        //return 'default_poblacion';
        return false;
    }
    if (poblacion.length > 0) {
        var regexp = /^[a-zA-Z/, -'()]*$/;
        return regexp.test(poblacion);
    }
    return false;
}

function validate_user() {
    var result = true;
    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var passwordreg = /^[0-9a-zA-Z]{2,20}$/;
    var string_reg = /^[A-Za-z ]{2,30}$/;

    var nombre = document.getElementById('nombre').value;
    var apellidos = document.getElementById('apellidos').value;
    var email = document.getElementById('email').value;
    var date_birthday = document.getElementById('date_birthday').value;
    var fecha_alta = document.getElementById('fecha_alta').value;
    var usuario = document.getElementById('usuario').value;
    var password = document.getElementById('password').value;
    var password2 = document.getElementById('password2').value;
    var estudios = new Array();
    var pais = $("#pais").val();
    var provincia = $("#provincia").val();
    var poblacion = $("#poblacion").val();

    var inputElements = document.getElementsByClassName("messageCheckbox");
    var j = 0;
    for (var i = 0; i < inputElements.length; i++) {
        if (inputElements[i].checked) {
            estudios[j] = inputElements[i].value;
            j++;
        }
    }
    $(".error").remove();

    if ($(".nombre").val() == "" || $(".nombre").val() == "Introduzca su nombre") {
        $(".nombre").focus().after("<span class='error'>Ingrese su nombre</span>");
        result = false;
        return false;
    } else if (!string_reg.test($(".nombre").val())) {
        $(".nombre").focus().after("<span class='error'>Mínimo 2 carácteres para el nombre y solo letras</span>");
        result = false;
        return false;

    } else if ($(".apellidos").val() == "" || $(".apellidos").val() == "Introduzca sus apellidos") {
        $(".apellidos").focus().after("<span class='error'>Ingrese sus apellidos</span>");
        result = false;
        return false;
    } else if (!string_reg.test($(".apellidos").val())) {
        $(".apellidos").focus().after("<span class='error'>Mínimo 2 carácteres para los apellidos y solo letras</span>");
        result = false;
        return false;

    } else if ($(".email").val() == "" || !emailreg.test($(".email").val()) || $(".email").val() == "Introduzca su email") {
        $(".email").focus().after("<span class='error'>Ingrese un email correcto</span>");
        result = false;
        return false;

    } else if ($(".date_birthday").val() == "" || $(".date_birthday").val() == "Introduzca su fecha de nacimiento") {
        $(".date_birthday").focus().after("<span class='error'>Introduzca su fecha de nacimiento</span>");
        result = false;
        return false;

    } else if ($(".fecha_alta").val() == "" || $(".fecha_alta").val() == "Introduzca su fecha de alta") {
        $(".fecha_alta").focus().after("<span class='error'>Introduzca su fecha de alta</span>");
        result = false;
        return false;

    } else if ($(".usuario").val() == "" || $(".usuario").val() == "Introduzca su usuario") {
        $(".usuario").focus().after("<span class='error'>Ingrese un usuario</span>");
        result = false;
        return false;
    } else if ($(".usuario").val().length < 2) {
        $(".usuario").focus().after("<span class='error'>Mínimo 2 carácteres para el usuario</span>");
        result = false;
        return false;

    } else if ($(".password").val() === "" || !passwordreg.test($(".password").val()) || $(".password").val() == "Introduzca su password") {
        $(".password").focus().after("<span class='error'>Introduzca un password correcto</span>");
        return false;

    } else if ($(".password2").val() === "" || $(".password").val() != $(".password2").val()) {
        $(".password2").focus().after("<span class='error'>Deben coincidir los passwords</span>");
        result = false;
        return false;
    }

    if (result) {

        if (provincia == null) {
            provincia = 'default_provincia';
        } else if (provincia.length == 0) {
            provincia = 'default_provincia';
        } else if (provincia === 'Selecciona una Provincia') {
            return 'default_provincia';
        }

        if (poblacion == null) {
            poblacion = 'default_poblacion';
        } else if (poblacion.length == 0) {
            poblacion = 'default_poblacion';
        } else if (poblacion === 'Selecciona una Poblacion') {
            return 'default_poblacion';
        }

        var data = {"nombre": nombre, "apellidos": apellidos, "email": email, "date_birthday": date_birthday,
            "fecha_alta": fecha_alta, "usuario": usuario, "password": password, "password2": password2, "estudios": estudios,
            "pais": pais, "provincia": provincia, "poblacion": poblacion}


        var data_users_JSON = JSON.stringify(data);
        $.post('modules/users/controller/controller_users.class.php', {alta_users_json: data_users_JSON},
        function (response) {

            if (response.success) {
                window.location.href = response.redirect;
            }
            ///// per a debuguejar loadmodel ////////
            //  alert(response.succes);
            //  console.log(response);
            //////////////////////////////////////////
            //}); //para debuguear
        }, "json")
                //.fail(function(xhr) {
                .fail(function (xhr, textStatus, errorThrown) {
                    //alert( "error" );

                    alert(xhr.status);
                    alert(textStatus);
                    alert(errorThrown);

                    if (xhr.status === 0) {
                        alert('Not connect: Verify Network.');
                    } else if (xhr.status == 404) {
                        alert('Requested page not found [404]');
                    } else if (xhr.status == 500) {
                        alert('Internal Server Error [500].');
                    } else if (textStatus === 'parsererror') {
                        alert('Requested JSON parse failed.');
                    } else if (textStatus === 'timeout') {
                        alert('Time out error.');
                    } else if (textStatus === 'abort') {
                        alert('Ajax request aborted.');
                    } else {
                        alert('Uncaught Error: ' + xhr.responseText);
                    }

                    $("#e_avatar").html(xhr.responseJSON.error_avatar);

                    if (!(xhr.responseJSON.success1)) {
                        $("#progress").hide();
                        $('.msg').text('').removeClass('msg_ok');
                        $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
                    }

                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.nombre !== undefined && xhr.responseJSON.error.nombre !== null) {
                            $("#e_nombre").text(xhr.responseJSON.error.nombre);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.apellidos !== undefined && xhr.responseJSON.error.apellidos !== null) {
                            $("#e_apellidos").text(xhr.responseJSON.error.apellidos);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.email !== undefined && xhr.responseJSON.error.email !== null) {
                            $("#e_email").text(xhr.responseJSON.error.email);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.date_birthday !== undefined && xhr.responseJSON.error.date_birthday !== null) {
                            $("#e_date_birthday").text(xhr.responseJSON.error.date_birthday);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.date_birthday !== undefined && xhr.responseJSON.error.fecha_alta !== null) {
                            $("#e_fecha_alta").text(xhr.responseJSON.error.fecha_alta);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.date_birthday !== undefined && xhr.responseJSON.error.usuario !== null) {
                            $("#e_usuario").text(xhr.responseJSON.error.usuario);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.password !== undefined && xhr.responseJSON.error.password !== null) {
                            $("#e_password").text(xhr.responseJSON.error.password);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.password2 !== undefined && xhr.responseJSON.error.password2 !== null) {
                            $("#e_password2").text(xhr.responseJSON.error.password2);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.password2 !== undefined && xhr.responseJSON.error.estudios !== null) {
                            $("#e_estudios").text(xhr.responseJSON.error.estudios);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.pais !== undefined && xhr.responseJSON.error.pais !== null) {
                            $("#e_pais").text(xhr.responseJSON.error.pais);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.provincia !== undefined && xhr.responseJSON.error.provincia !== null) {
                            $("#e_provincia").text(xhr.responseJSON.error.provincia);
                        }
                    }
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {
                        if (xhr.responseJSON.error.poblacion !== undefined && xhr.responseJSON.error.poblacion !== null) {
                            $("#e_poblacion").text(xhr.responseJSON.error.poblacion);
                        }
                    }
                });
    }
}


$(document).ready(function () {

//funció per a que no siga menor de edad
    $(".date_birthday").datepicker(
            {
                minDate: new Date(1900, 1 - 1, 1), maxDate: '-18Y',
                dateFormat: 'dd/mm/yy',
                defaultDate: new Date(1980, 1 - 1, 1),
                changeMonth: true,
                changeYear: true,
                yearRange: '-110:-18'
            }
    );

//funció per a que la fecha de alta no siga futura
    $(".fecha_alta").datepicker(
            {
                maxDate: "today",
                dateFormat: 'dd/mm/yy',
                defaultDate: "today",
                changeMonth: true,
                changeYear: true
            }
    );

//funció per a que el format del datepicker siga espanyol
    $.datepicker.regional['es'] = {
        closeText: 'Cerrar',
        prevText: '<Ant',
        nextText: 'Sig>',
        currentText: 'Hoy',
        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
        dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
        dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
        weekHeader: 'Sm',
        dateFormat: 'dd/mm/yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };


    // datepickers
    $("#date_birthday").datepicker();
    $("#fecha_alta").datepicker();



    //   $(this).LlenarLimpiarCampos(); //siempre que creemos un plugin debemos llamarlo, sino no funcionará

    $(".SubmitUsers").click(function () {
        validate_user();
    });

    $(".nombre, .apellidos, .email, .date_birthday, .fecha_alta, .usuario").keyup(function () {
        if ($(this).val() != "") {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".nombre").keyup(function () {
        if ($(this).val().length >= 2) {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".apellidos").keyup(function () {
        if ($(this).val().length >= 2) {
            $(".error").fadeOut();
            return false;
        }
    });

    var emailreg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    $(".email").keyup(function () {
        if ($(this).val() != "" && emailreg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".date_birthday").keyup(function () {
        if ($(this).val().length >= 2) {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".fecha_alta").keyup(function () {
        if ($(this).val().length >= 2) {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".usuario").keyup(function () {
        if ($(this).val().length >= 2) {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".password").keyup(function () {
        if ($(this).val().length >= 6) {
            $(".error").fadeOut();
            return false;
        }
    });

    $(".password2").keyup(function () {
        if ($(this).val().length >= 6) {
            $(".error").fadeOut();
            return false;
        }
    });
    var regexp = /^[a-zA-Z]*$/;
    $("#pais").keyup(function () {
        if (regexp.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    var regexp = /^[a-zA-Z/, -'()]*$/;
    $("#provincia").keyup(function () {
        if (regexp.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });
    var regexp = /^[a-zA-Z/, -'()]*$/;
    $("#poblacion").keyup(function () {
        if (regexp.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#progress").hide();


    $.get("modules/users/controller/controller_users.class.php?load_data=true",
            function (response) {
                //   alert(response.user);
                if (response.user === "") {
                    $("#nombre").val('');
                    $("#apellidos").val('');
                    $("#email").val('');
                    $("#date_birthday").val('');
                    $("#fecha_alta").val('');
                    $("#usuario").val('');
                    $("#password").val('');
                    $("#password2").val('');
                    $("#estudios").val('');
                    $("#pais").val('');
                    $("#provincia").val('');
                    $("#poblacion").val('');

                    $(this).LlenarLimpiarCampos();
                } else {
                    $("#nombre").attr("value", response.user.nombre);
                    $("#apellidos").attr("value", response.user.apellidos);
                    $("#email").attr("value", response.user.email);
                    $("#date_birthday").attr("value", response.user.date_birthday);
                    $("#fecha_alta").attr("value", response.user.fecha_alta);
                    $("#usuario").attr("value", response.user.usuario);
                    $("#password").attr("value", response.user.password);
                    $("#password2").attr("value", response.user.password2);
                    $("#estudios").attr("value", response.user.estudios);
                    $("#pais").attr("value", response.user.pais);
                    $("#provincia").attr("value", response.user.provincia);
                    $("#poblacion").attr("value", response.user.poblacion);

                }
            }, "json");

    Dropzone.autoDiscover = false;
    // Dropzone.options.myAwesomeDropzone = false;

    $("#dropzone").dropzone({
        url: "modules/users/controller/controller_users.class.php?upload=true",
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "Ha ocurrido un error en el server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                //alert(response);
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
            });
        },
        complete: function (file) {
            //if(file.status == "success"){
            //alert("El archivo se ha subido correctamente: " + file.name);
            //}
        },
        error: function (file) {
            //alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            $.ajax({
                type: "POST",
                url: "modules/users/controller/controller_users.class.php?delete=true",
                data: "filename=" + name,
                success: function (data) {
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(data);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                            //alert("Imagen eliminada: " + name);
                        } else {
                            false;
                        }
                    } else { //json.res == false, elimino la imagen también
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                        } else {
                            false;
                        }
                    }
                }
            });
        }
    });


    load_countries_v1();
    $("#provincia").empty();
    $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');
    $("#provincia").prop('disabled', true);
    $("#poblacion").empty();
    $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');
    $("#poblacion").prop('disabled', true);

    $("#pais").change(function () {
        var pais = $(this).val();
        var provincia = $("#provincia");
        var poblacion = $("#poblacion");

        if (pais !== 'ES') {
            provincia.prop('disabled', true);
            poblacion.prop('disabled', true);
            $("#provincia").empty();
            $("#poblacion").empty();
        } else {
            provincia.prop('disabled', false);
            poblacion.prop('disabled', false);
            load_provincias_v1();
        }//fi else
    });

    $("#provincia").change(function () {
        var prov = $(this).val();
        if (prov > 0) {
            load_poblaciones_v1(prov);
        } else {
            $("#poblacion").prop('disabled', false);
        }
    });
});


function load_countries_v2(cad) {
    $.getJSON(cad, function (data) {
        $("#pais").empty();
        $("#pais").append('<option value="" selected="selected">Selecciona un Pais</option>');

        $.each(data, function (i, valor) {
            $("#pais").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");
        });
    })
            .fail(function () {
                alert("error load_countries");
            });
}

function load_countries_v1() {
    $.get("modules/users/controller/controller_users.class.php?load_pais=true",
            function (response) {
                //console.log(response);
                if (response === 'error') {
                    load_countries_v2("resources/ListOfCountryNamesByName.json");
                } else {
                    load_countries_v2("modules/users/controller/controller_users.class.php?load_pais=true"); //oorsprong.org
                }
            })
            .fail(function (response) {
                load_countries_v2("resources/ListOfCountryNamesByName.json");
            });
}

function load_provincias_v2() {
    $.get("resources/provinciasypoblaciones.xml", function (xml) {
        $("#provincia").empty();
        $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');

        $(xml).find("provincia").each(function () {
            var id = $(this).attr('id');
            var nombre = $(this).find('nombre').text();
            $("#provincia").append("<option value='" + id + "'>" + nombre + "</option>");
        });
    })
            .fail(function () {
                alert("error load_provincias");
            });
}

function load_provincias_v1() { //provinciasypoblaciones.xml - xpath
    $.get("modules/users/controller/controller_users.class.php?load_provincias=true",
            function (response) {
                $("#provincia").empty();
                $("#provincia").append('<option value="" selected="selected">Selecciona una Provincia</option>');

                //alert(response);
                var json = JSON.parse(response);
                var provincias = json.provincias;
                //alert(provincias);
                //console.log(provincias);

                //alert(provincias[0].id);
                //alert(provincias[0].nombre);

                if (provincias === 'error') {
                    load_provincias_v2();
                } else {
                    for (var i = 0; i < provincias.length; i++) {
                        $("#provincia").append("<option value='" + provincias[i].id + "'>" + provincias[i].nombre + "</option>");
                    }
                }
            })
            .fail(function (response) {
                load_provincias_v2();
            });
}

function load_poblaciones_v2(prov) {
    $.get("resources/provinciasypoblaciones.xml", function (xml) {
        $("#poblacion").empty();
        $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');

        $(xml).find('provincia[id=' + prov + ']').each(function () {
            $(this).find('localidad').each(function () {
                $("#poblacion").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
            });
        });
    })
            .fail(function () {
                alert("error load_poblaciones");
            });
}

function load_poblaciones_v1(prov) { //provinciasypoblaciones.xml - xpath
    var datos = {idPoblac: prov};
    $.post("modules/users/controller/controller_users.class.php", datos, function (response) {
        //alert(response);
        var json = JSON.parse(response);
        var poblaciones = json.poblaciones;
        //alert(poblaciones);
        //console.log(poblaciones);
        //alert(poblaciones[0].poblacion);

        $("#poblacion").empty();
        $("#poblacion").append('<option value="" selected="selected">Selecciona una Poblacion</option>');

        if (poblaciones === 'error') {
            load_poblaciones_v2(prov);
        } else {
            for (var i = 0; i < poblaciones.length; i++) {
                $("#poblacion").append("<option value='" + poblaciones[i].poblacion + "'>" + poblaciones[i].poblacion + "</option>");
            }
        }
    })
            .fail(function () {
                load_poblaciones_v2(prov);
            });
}