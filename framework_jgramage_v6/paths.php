<?php


//SITE_ROOT   PHP
$path = $_SERVER['DOCUMENT_ROOT'] . '/framework_jgramage_v6';
define('SITE_ROOT', $path);

//SITE_PATH   JS
define('SITE_PATH', 'http://' . $_SERVER['HTTP_HOST'] . '/framework_jgramage_v6');

//CSS
define('CSS_PATH', SITE_PATH . '/view/css/');

//JS
define('JS_PATH', SITE_PATH . '/view/js/');

//PLUGIN
define('JS_PLUGINS', SITE_PATH . '/view/plugins/');

//log
define('LOG_DIR', SITE_ROOT . '/classes/log.class.singleton.php');
define('USER_LOG_DIR', SITE_ROOT . '/log/user/site_user_errors.log');
define('GENERAL_LOG_DIR', SITE_ROOT . '/log/general/site_general_errors.log');

//production
define('PRODUCTION', true);

//model
define('MODEL_PATH', SITE_ROOT . '/model/');

//view
define('VIEW_PATH_INC', SITE_ROOT . '/view/inc/');

//modules
define('MODULES_PATH',SITE_ROOT.'/modules/');
    
//resources
define('RESOURCES', SITE_ROOT . '/resources/');

//media
define('MEDIA_PATH', SITE_ROOT . '/media/');

//utils
define('UTILS', SITE_ROOT . '/utils/');

//model users
define('FUNCTIONS_USERS', SITE_ROOT . '/modules/users/utils/');
define('MODEL_PATH_USERS', SITE_ROOT . '/modules/users/model/');
define('DAO_USERS', SITE_ROOT . '/modules/users/model/DAO/');
define('BLL_USERS', SITE_ROOT . '/modules/users/model/BLL/');
define('MODEL_USERS', SITE_ROOT . '/modules/users/model/model/');
define('USERS_JS_PATH', SITE_PATH . '/modules/users/view/js/');
define('USERS_CSS_PATH', SITE_PATH . '/modules/users/view/css/');

//model products
define('FUNCTIONS_PRODUCTS', SITE_ROOT . '/modules/products/utils/');
define('MODEL_PATH_PRODUCTS', SITE_ROOT . '/modules/products/model/');
define('DAO_PRODUCTS', SITE_ROOT . '/modules/products/model/DAO/');
define('BLL_PRODUCTS', SITE_ROOT . '/modules/products/model/BLL/');
define('MODEL_PRODUCTS', SITE_ROOT . '/modules/products/model/model/');
define('UTILS_PRODUCTS', SITE_ROOT . '/modules/products/utils/');
define('PRODUCTS_JS_LIB_PATH', SITE_PATH . '/modules/products/view/lib/');
define('PRODUCTS_JS_PATH', SITE_PATH . '/modules/products/view/js/');
define('PRODUCTS_CSS_PATH', SITE_PATH . '/modules/products/view/css/');

//Activacio URL amigables
define('URL_AMIGABLES', TRUE);