<?php

class controller_products {

    function __construct() {

        //  include(SITE_ROOT . "paths.php");
        include(UTILS_PRODUCTS . "utils.inc.php");
        include LOG_DIR;
        /*
        include(UTILS . "filters.inc.php");
        include(UTILS . "utils.inc.php");
        include(UTILS . "response_code.inc.php");
        include(UTILS . "common.inc.php");  */

        $_SESSION['module'] = "products";
    }

    function list_products() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/products/view/', 'list_products.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function autocomplete_products() {
        if ((isset($_POST["autocomplete"])) && ($_POST["autocomplete"] === "true")) {

            set_error_handler('ErrorHandler');
            try {
               
                $nom_productos = loadModel(MODEL_PRODUCTS, "product_model", "order_by_name");
                //throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($nom_productos) {
                $jsondata["nom_productos"] = $nom_productos;
                echo json_encode($jsondata);
                exit;
            } else {
                //if($nom_productos){ //que lance error si no hay productos
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    function nom_products() {
        if (($_POST["nom_product"])) {
            //filtrar $_GET["nom_product"]
            $result = filter_string($_POST["nom_product"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }

            set_error_handler('ErrorHandler');
            try {
            
                $product = loadModel(MODEL_PRODUCTS, "product_model", "distinct_like", $criteria);
                //throw new Exception(); //que entre en el catch
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($product) {
                $jsondata["product_autocomplete"] = $product[0];
                echo json_encode($jsondata);
                exit;
            } else {
                //if($producto){{ //que lance error si no existe el producto
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    function count_products() {
        if (($_POST["count_product"])) {

            //filtrar $_GET["count_product"]
            $result = filter_string($_POST["count_product"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }

            // set_error_handler('ErrorHandler');
            try {
                //   throw new Exception();
                $resultado = loadModel(MODEL_PRODUCTS, "product_model", "count_like", $criteria);
                $total_rows = $resultado;
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($total_rows) {
                $jsondata["num_products"] = $total_rows[0];
                echo json_encode($jsondata);
                exit;
            } else {
                //if($total_rows){ //que lance error si no existe el producto
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    function num_pages_products() {
        if ((isset($_POST["num_pages"])) && ($_POST["num_pages"] == true)) {

            if (isset($_POST["keyword"])) {
                $result = filter_string($_POST["keyword"]);
                if ($result['resultado']) {
                    $criteria = $result['datos'];
                } else {
                    $criteria = '';
                }
            } else {
                $criteria = '';
            }

            set_error_handler('ErrorHandler');
            $item_per_page = 6;
            $pages = 0;
            try {
                $arrValue = loadModel(MODEL_PRODUCTS, "product_model", "total_products", $criteria);
                $get_total_rows = $arrValue[0]["total"];
                $pages = ceil($get_total_rows / $item_per_page); //break total records into pages
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();

            if ($get_total_rows) {
                $jsondata["pages"] = $pages;
                echo json_encode($jsondata);
                exit;
            } else {
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    function view_error_true() {
        if ((isset($_POST["view_error"])) && ($_POST["view_error"] == "true")) {
            showErrorPage(0, "ERROR - 503 BD Unavailable");
        }
    }

    function view_error_false() {
        if ((isset($_POST["view_error"])) && ($_POST["view_error"] == "false")) {
            //showErrorPage(0, "ERROR - 404 NO PRODUCTS");
            showErrorPage(3, "RESULTS NOT FOUND");
        }
    }

    function idProduct() {
        if ($_POST["idProducto"]) {
            //  $arrValue = null;
            $result = filter_num_int($_POST["idProducto"]);
            if ($result['resultado']) {
                $id = $result['datos'];
            } else {
                $id = 1;
            }

            set_error_handler('ErrorHandler');
            try {
                //    $path_model = SITE_ROOT . '/modules/products/model/model/';
                $arrValue = loadModel(MODEL_PRODUCTS, "product_model", "details_products", $id);
            } catch (Exception $e) {
                showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
            }
            restore_error_handler();


            if ($arrValue) {
                $jsondata["product"] = $arrValue;
                //    $jsondata["product"] = $arrValue[0];
                echo json_encode($jsondata);
                exit;
            } else {
                showErrorPage(2, "ERROR - 404 NO DATA", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    function obtain_products() {
        if (isset($_POST["page_num"])) {
            $result = filter_num_int($_POST["page_num"]);
            if ($result['resultado']) {
                $page_number = $result['datos'];
            }
        } else {
            $page_number = 1;
        }

        if (isset($_POST["keyword"])) {
            $result = filter_string($_POST["keyword"]);
            if ($result['resultado']) {
                $criteria = $result['datos'];
            } else {
                $criteria = '';
            }
        } else {
            $criteria = '';
        }


        if (isset($_POST["keyword"])) {
            $result = filter_string($_POST["keyword"]);
            if ($result['resultado']) {
                $criteria1 = $result['datos'];
            } else {
                $criteria1 = '';
            }
        } else {
            $criteria1 = '';
        }

        if (isset($_POST["keyword"])) {
            $criteria = $criteria1;
        }

        set_error_handler('ErrorHandler');
        try {

            $item_per_page = 6;

            $position = (($page_number - 1) * $item_per_page);
            $arrArgument = array(
                'position' => $position,
                'item_per_page' => $item_per_page,
                'criteria' => $criteria
            );
            $arrValue = loadModel(MODEL_PRODUCTS, "product_model", "total_pages", $arrArgument);
        } catch (Exception $e) {
            showErrorPage(0, "ERROR - 503 BD Unavailable");
        }
        restore_error_handler();

        if ($arrValue) {
            paint_template_products($arrValue);
        } else {
            showErrorPage(0, "ERROR - 404 NO PRODUCTS");
        }
    }
}