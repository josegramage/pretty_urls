<?php

// require(BLL_PRODUCTS . "productBLL.class.singleton.php");

class product_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = product_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    function list_products() {
        return $this->bll->list_products_bll();
    }

    function details_products($id) {    
        return $this->bll->details_products_bll($id);
    }
    
    function n_pages() {
        return $this->bll->n_pages_bll();
    }

    function total_pages($arrArgument) {
        return $this->bll->total_pages_bll($arrArgument);
    }
    
    function order_by_name() {
        return $this->bll->order_by_name_bll();
    }

    function distinct_like($criteria) {
        return $this->bll->distinct_like_bll($criteria);
    }

    function count_like($criteria) {
        return $this->bll->count_like_bll($criteria);
    }
    
    function total_products($criteria) {
        return $this->bll->total_products_bll($criteria);
    }
}