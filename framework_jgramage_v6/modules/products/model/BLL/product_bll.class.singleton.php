<?php

 //   require (MODEL_PATH . "Db.class.singleton.php");
 //   require(DAO_PRODUCTS . "productDAO.class.singleton.php");

class product_bll {

    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = product_dao::getInstance();
        $this->db = db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    function list_products_bll() {
        return $this->dao->list_products_dao($this->db);
    }

    function details_products_bll($id) {
        return $this->dao->details_products_dao($this->db, $id);
    }

    function n_pages_bll() {
        return $this->dao->n_pages_dao($this->db);
    }

    function total_pages_bll($arrArgument) {
        return $this->dao->total_pages_dao($this->db, $arrArgument);
    }

    function order_by_name_bll() {
        return $this->dao->order_by_name_dao($this->db);
    }

    function distinct_like_bll($criteria) {
        return $this->dao->distinct_like_dao($this->db, $criteria);
    }

    function count_like_bll($criteria) {
        return $this->dao->count_like_dao($this->db, $criteria);
    }
    
    function total_products_bll($criteria) {
        return $this->dao->total_products_dao($this->db, $criteria);
    }

}
