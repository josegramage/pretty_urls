<script type="text/javascript" src="<?php echo PRODUCTS_JS_PATH ?>list_products.js" ></script>
<script type="text/javascript" src="<?php echo PRODUCTS_JS_LIB_PATH ?>jquery.bootpag.min.js"></script>
<link href="<?php echo PRODUCTS_CSS_PATH ?>products.css" rel="stylesheet" /> 


<div id="main">
    <div id="content" class="container"</div>

    <center>
        <form name="search_prod" id="search_prod" class="search_prod">
            <input type="text" value="" placeholder="Search Product ..." class="input_search" id="keyword" list="datalist">
            <!-- <div id="results_keyword"></div> -->
            <input name="Submit" id="Submit" class="button_search" type="button" />

        </form>
    </center>

    <div id="results"></div>

    <center>
        <div class="pagination"></div>
    </center>

    <!-- modal window details_product -->
    <div id="product">
        <div class="row text-center pad-row">
            <div id="img_product"></div>
            <br><br>
            <h4> <strong> <div id="nom_product"></div> </strong> </h4>
            <p> <div id="desc_product"></div> </p>
            <h9> <strong> <div id="price_product"></div> </strong> </h9>
            <p> <div id="back_list"></div> </p>
        </div>
    </div>
</div>
</div>