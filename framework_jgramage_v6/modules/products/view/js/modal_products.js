$(document).ready(function () {
    $('.nombre_producto').click(function () {
        var id = this.getAttribute('id');

        //  $.get("modules/products/controller/controller_products.class.php?idProducto=" + id, function (data, status) {
         $.post("/framework_jgramage_v6/products/idProduct/", {'idProducto': id}, function (data, status) {   
           
            var json = JSON.parse(data);
            var product = json.product;
            var datos = product[0];
            //alert(product.nombre);
            //console.log(product);

            $('#results').html('');
            $('.pagination').html('');

            $('#img_product').html('<img src="../' + datos.img + '" height="75" width="75"> ');
            $('#nom_product').html(datos.nombre);
            $('#desc_product').html(datos.descripcion);
            $('#price_product').html("Precio: " + datos.precio + " €");
            $('#back_list').html('<a href="/framework_jgramage_v6/products/list_products">Back To List Products</a>');
        })
                .fail(function (xhr) {
                    if (xhr.status === 404) {
                        // $("#results").load("modules/products/controller/controller_products.class.php?view_error=false"); //view_error=false
                        $("#results").load("/framework_jgramage_v6/products/view_error_false/", {'view_error':false});
                    } else {
                        // $("#results").load("modules/products/controller/controller_products.class.php?view_error=true");
                        $("#results").load("/framework_jgramage_v6/products/view_error_false/", {'view_error':true});
                    }
                    $('.pagination').html('');

                    $('#img_product').html('');
                    $('#nom_product').html('');
                    $('#desc_product').html('');
                    $('#price_product').html('');

                    $('#keyword').val('');
                });
    });
}); 