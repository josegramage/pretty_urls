function validate_search(search_value) {
    if (search_value.length > 0) {
        var regexp = /^[a-zA-Z0-9 .]*$/;
        return regexp.test(search_value);
    }
    return false;
}

function refresh() {

    $('.pagination').html = '';
    $('.pagination').val = '';
}


function search_not_empty(keyword) {
    ////////////////////////// keyword.length >= 1 /////////////////////////
    //  $.get("modules/products/controller/controller_products.class.php?num_pages=true&keyword=" + keyword, function (data, status) {
//    $.get("index.php?module=products&function=num_pages_products&num_pages=true&keyword=" + keyword, function (data, status) {
    $.post("/framework_jgramage_v6/products/num_pages_products/", {'num_pages': true, 'keyword': keyword}, function (data, status) {  
        var json = JSON.parse(data);
        var pages = json.pages;
        //	alert("num_pages = "+pages);

        //   $("#results").load("modules/products/controller/controller_products.class.php?keyword=" + keyword);
     //   $("#results").load("index.php?module=products&function=obtain_products&keyword=" + keyword);
        $("#results").load("/framework_jgramage_v6/products/obtain_products", {'keyword': keyword});

        if (pages !== 0) {
            refresh();
            $(".pagination").bootpag({
                total: pages,
                page: 1,
                maxVisible: 3,
                next: 'next',
                prev: 'prev'
            }).on("page", function (e, num) {
                //alert(num);
                e.preventDefault();
                //$("#results").load("modules/products/controller/controller_products.class.php", {'page_num': num, 'keyword': keyword});
            //    $("#results").load("index.php?module=products&function=obtain_products", {'page_num': num, 'keyword': keyword});
                  $("#results").load("/framework_jgramage_v6/products/obtain_products", {'page_num': num, 'keyword': keyword});
            reset();
            });
        } else {
            //    $("#results").load("modules/products/controller/controller_products.class.php?view_error=false"); //view_error=false
            $("#results").load("/framework_jgramage_v6/products/view_error_false/", {'view_error': false});
            $('.pagination').html('');
            reset();
        }
        reset();

    }).fail(function (xhr) {
        //    $("#results").load("modules/products/controller/controller_products.class.php?view_error=true");
        $("#results").load("/framework_jgramage_v6/products/view_error_true/", {'view_error': true});
        $('.pagination').html('');
        reset();
    });
}

function search_empty() {
    ////////////////////////// keyword.length == 0 /////////////////////////

    //  $.get("modules/products/controller/controller_products.class.php?num_pages=true", function (data, status) {
    $.post("/framework_jgramage_v6/products/num_pages_products/", {'num_pages': true}, function (data, status) {
        var json = JSON.parse(data);
        var pages = json.pages;
        //alert("num_pages = "+pages);

        //$("#results").load("modules/products/controller/controller_products.class.php"); //load initial records
        $("#results").load("/framework_jgramage_v6/products/obtain_products/");

        $(".pagination").bootpag({
            total: pages,
            page: 1,
            maxVisible: 3,
            next: 'next',
            prev: 'prev'
        }).on("page", function (e, num) {
            //alert(num);
            e.preventDefault();
            //  $("#results").load("modules/products/controller/controller_products.class.php", {'page_num': num});
            $("#results").load("/framework_jgramage_v6/products/obtain_products/", {'page_num': num});
            reset();
        });

        reset();

    }).fail(function (xhr) {
        //   $("#results").load("modules/products/controller/controller_products.class.php?view_error=true");
        $("#results").load("/framework_jgramage_v6/products/view_error_true/", {'view_error': true});
        $('.pagination').html('');
        reset();
    });
}

function search_product(keyword) {
    //   $.get("modules/products/controller/controller_products.class.php?nom_product=" + keyword, function (data, status) {
    $.post("/framework_jgramage_v6/products/nom_products/", {'nom_product': keyword}, function (data, status) {
        var json = JSON.parse(data);
        var product = json.product_autocomplete;
        //alert(json.product_autocomplete);
        //alert(product.nombre);

        $('#results').html('');
        $('.pagination').html('');

        $('#img_product').html('<img src="' + product.img + '" height="75" width="75"> ');
        $('#nom_product').html(product.nombre);
        $('#desc_product').html(product.descripcion);
        $('#price_product').html("Precio: " + product.precio + " €");
        $('#back_list').html('<a href="/framework_jgramage_v6/products/list_products">Back To List Products</a>');

    }).fail(function (xhr) {
        //    $("#results").load("modules/products/controller/controller_products.class.php?view_error=false");
        $("#results").load("/framework_jgramage_v6/products/view_error_false/", {'view_error': false});
        $('.pagination').html('');
        reset();
    });
}

function count_product(keyword) {

    //  $.get("modules/products/controller/controller_products.class.php?count_product=" + keyword, function (data, status) {
    $.post("/framework_jgramage_v6/products/count_products/", {'count_product': keyword}, function (data, status) {
        var json = JSON.parse(data);
        var num_products = json.num_products;
        var product = num_products.total;

        if (product == 0) {

        //$("#results").load("modules/products/controller/controller_products.class.php?view_error=false"); //view_error=false
            $("#results").load("/framework_jgramage_v6/products/view_error_false/", {'view_error': false});
            $('.pagination').html('');
            reset();
        }
        if (product == 1) {
            search_product(keyword);
        }
        if (product > 1) {
            search_not_empty(keyword);
        }
    }).fail(function () {
        //$("#results").load("modules/products/controller/controller_products.class.php?view_error=true"); //view_error=false
        $("#results").load("/framework_jgramage_v6/products/view_error_true/", {'view_error':true});
        $('.pagination').html('');
        reset();
    });
}


function reset() {
    $('#img_product').html('');
    $('#nom_product').html('');
    $('#desc_product').html('');
    $('#price_product').html('');

    $('#keyword').val('');
}

$(document).ready(function () {
    ////////////////////////// inici carregar pàgina /////////////////////////
    if (getCookie("search")) {
        var keyword = getCookie("search");
        count_product(keyword);
        $("#keyword").val(keyword);
        setCookie("search", "", 1);
    } else {
        search_empty();
    }

    $("#search_prod").submit(function (e) {
        var keyword = document.getElementById('keyword').value;
        var v_keyword = validate_search(keyword);

        if (!v_keyword) {
            setCookie("search", keyword, 1);
        }

        location.reload(true);

        //si no ponemos la siguiente línea, el navegador nos redirecciona a index.php
        e.preventDefault(); //STOP default action
    });

    $('#Submit').click(function () {
        var keyword = document.getElementById('keyword').value;
        var v_keyword = validate_search(keyword);

        if (!v_keyword) {
            search_empty();
        } else {
            count_product(keyword);
        }
    });

  //  $.get("modules/products/controller/controller_products.class.php?autocomplete=true", function (data, status) {
       $.post("/framework_jgramage_v6/products/autocomplete_products/", {'autocomplete':true}, function(data, status){
	var json = JSON.parse(data);
        var nom_productos = json.nom_productos;
        //	alert(nom_productos[0].nombre);
        //console.log(nom_productos);

        var suggestions = new Array();
        for (var i = 0; i < nom_productos.length; i++) {
            suggestions.push(nom_productos[i].nombre);
        }
        //alert(suggestions);
        //console.log(suggestions);

        $("#keyword").autocomplete({
            source: suggestions,
            minLength: 1,
            select: function (event, ui) {
                //alert(ui.item.label);

                var keyword = ui.item.label;
                count_product(keyword);
            }
        });
    }).fail(function (xhr) {
        if (xhr.status == "404")
        // $("#results").load("modules/products/controller/controller_products.class.php?view_error=false"); //view_error=false
        $("#results").load("/framework_jgramage_v6/products/view_error_false/", {'view_error':false});
	$('.pagination').html('');
        reset();
    });

});
//Function to setCookie
function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
}

//Function to getCookie
function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1);
        if (c.indexOf(name) == 0)
            return c.substring(name.length, c.length);
    }
    return 0;
}