<?php

function paint_template_error($message) {

    $log = log::getInstance();
    $log->add_log_general("error paint_template_error", "products", "response " . http_response_code()); //$text, $controller, $function
    $log->add_log_user("error paint_template_error", "", "products", "response " . http_response_code()); //$msg, $username = "", $controller, $function

    print ("<section> \n");
    print ("<div class='container'> \n");
    print ("<div class='row text-center pad-row'> \n");

    print ("<h1>ERROR 404 - " . $message . "</h1> \n");
    print ("<br><br><br><br> \n");

    print ("<h2>The following error occurred:</h2> \n");
    print ("<p>The requested URL was not found on this server.</p> \n");
    print ("<P>Please check the URL or contact the <!--WEBMASTER//-->webmaster<!--WEBMASTER//-->.</p> \n");
    print (' <p><a class="btn btn-primary" href="http://51.254.113.182/framework_jgramage_v6/index.php?module=main">Return to HomePage</a></p>');
    print ("<br><br><br><br> \n");
    print ("<p>Powered by <a href='http://www.ispconfig.org'>ISPConfig</a></p> \n");

    print ("</div> \n");
    print ("</div> \n");
    print ("</section> \n");
}

function paint_template_products($resultado) {
       print ("<script type='text/javascript' src='" . PRODUCTS_JS_PATH . "modal_products.js' ></script> \n");
    print ("<section> \n");
    print ("<div class='container'> \n");
    print ("<div class='row text-center pad-row'> \n");

    foreach ($resultado as $productos) {
        print ("<div class='col-md-4  col-sm-4'> \n");
        print ("<img src='../" . $productos['img'] . "' alt='product' height='70' width='70'> \n");
        //  print ("<img src='view/images/product.jpg' alt='product' height='70' width='70'> \n");
        print ("<h4> <strong>" . $productos['nombre'] . "</strong> </h4> \n");

        print ("<p><textarea cols='30'>" . $productos['descripcion'] . "</textarea></p> \n");

        print ("<h5> <strong>Price: " . $productos['precio'] . "€</strong> </h5> \n");

        //   $callback = "index.php?module=products&idProducto=" . $productos['id'];
        // print ("<a href='" . $callback . "' class='btn btn-primary' >Read Details</a> \n");

        echo "<div id='" . $productos['id'] . "' class='nombre_producto'>Read Details </div>";
        print ("<br><br><br><br> \n");
        print ("</div> \n");
    }
    print ("</div> \n");
    print ("</div> \n");
    print ("</section> \n");
}

function paint_template_search($message) {
    $log = log::getInstance();
    $log->add_log_general("error paint_template_search", "products", "response " . http_response_code()); //$text, $controller, $function
    $log->add_log_user("error paint_template_search", "", "products", "response " . http_response_code()); //$msg, $username = "", $controller, $function

    print ("<section> \n");
    print ("<div class='container'> \n");
    print ("<div class='row text-center pad-row'> \n");

    print ("<h2>" . $message . "</h2> \n");
    print ("<br><br><br><br> \n");

    print ("</div> \n");
    print ("</div> \n");
    print ("</section> \n");
}
