<?php

function validate_user($value) {

    $error = array();
    $valido = true;
    $filtro = array(
        'nombre' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{2,30}$/')
        ),
        'apellidos' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{2,30}$/')
        ),
        'email' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'validatemail'
        ),
        'date_birthday' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
        ),
        'fecha_alta' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/\d{2}.\d{2}.\d{4}$/')
        ),
        'usuario' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^.{2,20}$/')
        ),
        'password' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{2,30}$/')
        ),
        'password2' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{2,30}$/')
        ),
        'pais' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z_]*$/')
        )
        ,
        'provincia' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-zA-Z0-9, _]*$/')
        )
        ,
        'poblacion' => array(
            'filter' => FILTER_CALLBACK,
            'options' => 'validate_poblacion'
        )
    );

    $resultado = filter_var_array($value, $filtro);

    $resultado['estudios'] = $value['estudios'];

    if ($resultado != null && $resultado) {



        if (!$resultado['nombre']) {
            $error['nombre'] = "El nombre debe tener de 2 a 30 caracteres";
            $resultado['nombre'] = $value['nombre'];
            $valido = false;
        } if (!$resultado['apellidos']) {
            $error['apellidos'] = "Los apellidos debe tener de 2 a 30 caracteres";
            $resultado['apellidos'] = $value['apellidos'];
            $valido = false;
        } if (!$resultado['email']) {
            $error['email'] = 'El email debe contener de 5 a 50 caracteres y debe ser un email valido';
            $resultado['email'] = $value['email'];
            $valido = false;
        } if (!$resultado['date_birthday']) {
            $error['date_birthday'] = 'Formato fecha dd/mm/yyyy';
            $resultado['date_birthday'] = $value['date_birthday'];
            $valido = false;
        } if ($dates = mayor_edad($value['date_birthday'])) {
            $error['date_birthday'] = 'Debe ser mayor de 18 anyos';
            $valido = false;
        } if ($dates = validar_date($value['fecha_alta'])) {
            $error['fecha_alta'] = 'La fecha de alta no puede ser posterior a hoy';
            $valido = false;
        } if (!$resultado['fecha_alta']) {
            $error['fecha_alta'] = 'Formato fecha dd/mm/yyyy';
            $resultado['fecha_alta'] = $value['fecha_alta'];
            $valido = false;
        } if (!$resultado['usuario']) {
            $error['usuario'] = 'El usuario debe tener de 4 a 20 caracteres';
            $resultado['usuario'] = $value['usuario'];
            $valido = false;
        } if ((!$resultado['password'] || $resultado['password'] != $value['password2'])) {
            $error['password'] = 'El password debe tener de 6 a 12 caracteres y las dos contrasenyas deben ser iguales';
            $resultado['password'] = $value['password'];
            $valido = false;
        } if ((!$resultado['password2'] || $resultado['password2'] != $value['password'])) {
            $error['password2'] = 'El password debe tener de 6 a 12 caracteres y las dos contrasenyas deben ser iguales';
            $resultado['password2'] = $value['password2'];
            $valido = false;
        } if ((count($value['estudios'])) < 1) {
            $error['estudios'] = 'Escoja al menos una opcion';
            $resultado['estudios'] = $value['estudios'];
            $valido = false;
            
        }    if (!$resultado['pais']) {
            $error['pais'] = 'pais introducido no válido';
            $resultado['pais'] = $value['pais'];
            $valido = false;
        }
        if (!$resultado['provincia']) {
            $error['provincia'] = 'provincia introducida no válida';
            $resultado['provincia'] = $value['provincia'];
            $valido = false;
        }
        if (!$resultado['poblacion']) {
            $error['poblacion'] = 'poblacion introducida no válida';
            $resultado['poblacion'] = $value['poblacion'];
            $valido = false;
        }
            
            //   } else {
            //     $resultado['estudios'] = $_POST['estudios'];
        
    } else {
        $valido = false;
    }
    return $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);
}

function validatemail($email) {
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }
    return false;
}

function mayor_edad($date_birthday) {
    $mayoria_edad = 18;
    if (is_string($date_birthday)) {
        $date_birthday = strtotime($date_birthday);
    }
    if (time() - $date_birthday < $mayoria_edad * 3153600) {
        return true;
    }
    return false;
}

/*
  function validar_password($password) {
  if (strlen($password) < 5) {
  $error = "passwd debe tener al menos 6 caracteres";
  return false;
  }
  if (strlen($password) > 16) {
  $error = "passwd no puede tener más de 16 caracteres";
  return false;
  }
  if (!preg_match('`[a-z]`', $password)) {
  $error = "passwd debe tener al menos una letra minúscula";
  return false;
  }
  if (!preg_match('`[A-Z]`', $password)) {
  $error = "passwd debe tener al menos una letra mayúscula";
  return false;
  }
  if (!preg_match('`[0-9]`', $password)) {
  $error = "passwd debe tener al menos un caracter numérico";
  return false;
  }
  return true;
  }
 */

function validar_date($date) {
    $array = explode("/", $date);
    $day = $array[1];
    $month = $array[0];
    $year = $array[2];

    if (!checkdate($month, $day, $year)) {
        
    } else {
        $today = strtotime("now");
        if (strtotime($date) < $today) {
            return false;
        }
    }
}

function calculaedad($date_birthday) {
    list($mes, $dia, $ano) = explode("/,$date_birthday");
    $mes_diferencia = date("a") - $mes;
    $dia_diferencia = date("d") - $dia;
    $ano_diferencia = date("y") - $ano;
    if ($dia_diferencia < 0 || $mes_diferencia < 0)
        $ano_diferencia--;
    return $ano_diferencia;
}

function validate_poblacion($poblacion){
			//$poblacion = addslashes($poblacion);
			$poblacion = filter_var($poblacion, FILTER_SANITIZE_STRING);
			return $poblacion;
	}