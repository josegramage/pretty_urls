<?php

//require(BLL_USERS . "userBLL.class.singleton.php");

class user_model {

    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = user_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_user($arrArgument) {
        return $this->bll->create_user_bll($arrArgument);
    }

    public function obtain_paises($url) {
        return $this->bll->obtain_paises_bll($url);
    }

    public function obtain_provincias() {
        return $this->bll->obtain_provincias_bll();
    }

    public function obtain_poblaciones($arrArgument) {
        return $this->bll->obtain_poblaciones_bll($arrArgument);
    }

}
