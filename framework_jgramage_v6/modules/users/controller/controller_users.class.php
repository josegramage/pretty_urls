<?php

class controller_users {

    public function __construct() {

        include(FUNCTIONS_USERS . "functions_users.inc.php");
        include(UTILS . "upload.php");
        include LOG_DIR;

        $_SESSION['module'] = "users";
    }

    public function form_users() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        echo '<br><br><br><br><br><br><br>';
        loadView('modules/users/view/', 'form_users.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    public function results_user() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        echo '<br><br><br><br><br><br><br>';
        loadView('modules/users/view/', 'results_user.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    public function upload_users() {
        if ((isset($_POST["upload"])) && ($_POST["upload"] == true)) {
            $result_avatar = upload_files();
            $_SESSION['result_avatar'] = $result_avatar;
            //echo debug($_SESSION['result_avatar']); //se mostraría en alert(response); de dropzone.js
        }
    }

    public function alta_users() {
        if (isset($_POST['alta_users_json'])) {
            $jsondata = array();

            $usersJSON = json_decode($_POST["alta_users_json"], true);
            //    echo debug($usersJSON);
            $result = validate_user($usersJSON);
            //	echo debug($result); //se mostraría en alert(response); de $.post('pages/controller_users.pp ...

            if (empty($_SESSION['result_avatar'])) {
                $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => './media/default-avatar.png');
            }
            //echo debug($_SESSION['result_avatar']); //se mostraría en alert(response); de $.post('pages/controller_users.pp ...

            $result_avatar = $_SESSION['result_avatar'];
            //$_SESSION['result_avatar'] = array(); ///// 
            //echo debug($result);
            //echo debug($result_avatar);

            if (($result['resultado']) && ($result_avatar['resultado'])) {
                $arrArgument = array(
                    'nombre' => $result['datos']['nombre'],
                    'apellidos' => $result['datos']['apellidos'],
                    'email' => $result['datos']['email'],
                    'date_birthday' => $result['datos']['date_birthday'],
                    'fecha_alta' => $result['datos']['fecha_alta'],
                    'usuario' => $result['datos']['usuario'],
                    'password' => $result['datos']['password'],
                    'estudios' => $result['datos']['estudios'],
                    'pais' => strtoupper($result['datos']['pais']),
                    'provincia' => strtoupper($result['datos']['provincia']),
                    'poblacion' => strtoupper($result['datos']['poblacion']),
                    'avatar' => $result_avatar['datos']
                );

                $_SESSION['result_avatar'] = array();

                ///////////////// insert into BD begin ////////////////////////

                $arrValue = false;

                set_error_handler('ErrorHandler');
                try {
                    $arrValue = loadModel(MODEL_USERS, "user_model", "create_user", $arrArgument);
                } catch (Exception $e) {
                    $arrValue = false;
                }
                restore_error_handler();

                if ($arrValue) {
                    $mensaje = "Su registro se ha efectuado correctamente, para finalizar compruebe que ha recibido un correo de validacion y siga sus instrucciones";
                    // else
                    //     $mensaje = "No se ha podido realizar su alta. Intentelo mas tarde";
                    /////////////////////////////////////////////////////////

                    $_SESSION['user'] = $arrArgument;
                    $_SESSION['msje'] = $mensaje;
                    $url = "/framework_jgramage_v6/users/results_user"; //index.php?module=formulary&function=registroprod";
                    
                           
                    $jsondata["success"] = true;
                    $jsondata["redirect"] = $url;
                    echo json_encode($jsondata);
                    exit;
                } else {
                    $mensaje = "No se ha podido realizar su alta. Intentelo mas tarde";
                    showErrorPage(1, "", 'HTTP/1.0 503 Service Unavailable', 503);
                }
            } else {
                $jsondata["success"] = false;
                $jsondata["error"] = $result['error'];
                $jsondata["error_avatar"] = $result_avatar['error'];

                $jsondata["success1"] = false;
                if ($result_avatar['resultado']) {
                    $jsondata["success1"] = true;
                    $jsondata["img_avatar"] = $result_avatar['datos'];
                }

                header('HTTP/1.0 404 Not Found', true, 404);
                echo json_encode($jsondata);
                //    exit;
            }
        }
    }

    public function delete_users() {
        if (isset($_POST["delete"]) && $_POST["delete"] == true) {
            $_SESSION['result_avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }
    }

    public function load_users() {
        if (isset($_POST["load"]) && $_POST["load"] == true) {
            $jsondata = array();
            if (isset($_SESSION['user'])) {
                //echo debug($_SESSION['user']);
                $jsondata["user"] = $_SESSION['user'];
            }
            if (isset($_SESSION['msje'])) {
                //echo $_SESSION['msje'];
                $jsondata["msje"] = $_SESSION['msje'];
            }
            if ($jsondata) {
                close_session(); ////////////////////////// en utils/utils.inc.php
                echo json_encode($jsondata);
                exit;
            } else {
                close_session(); ////////////////////////// en utils/utils.inc.php
                showErrorPage(1, "", 'HTTP/1.0 404 Not Found', 404);
            }
        }
    }

    public function load_data_users() {
        if ((isset($_POST["load_data"])) && ($_POST["load_data"] == true)) {
            $jsondata = array();

            if (isset($_SESSION['user'])) {
                $jsondata["user"] = $_SESSION['user'];
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["user"] = "";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    public function load_pais_users() {
        if ((isset($_POST["load_pais"])) && ($_POST["load_pais"] == true)) {
            $json = array();

            $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';

            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_USERS, "user_model", "obtain_paises", $url);
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();

            if ($json) {
                echo $json;
                exit;
            } else {
                $json = "error";
                echo $json;
                exit;
            }
        }
    }

    public function load_provincias_users() {
        if ((isset($_POST["load_provincias"])) && ($_POST["load_provincias"] == true)) {
            $jsondata = array();
            $json = array();

            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_USERS, "user_model", "obtain_provincias");
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();

            if ($json) {
                $jsondata["provincias"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["provincias"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    public function load_poblaciones_users() {
        if (isset($_POST['idPoblac'])) {
            $jsondata = array();
            $json = array();

            set_error_handler('ErrorHandler');
            try {
                $json = loadModel(MODEL_USERS, "user_model", "obtain_poblaciones", $_POST['idPoblac']);
            } catch (Exception $e) {
                $json = array();
            }
            restore_error_handler();

            if ($json) {
                $jsondata["poblaciones"] = $json;
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["poblaciones"] = "error";
                echo json_encode($jsondata);
                exit;
            }
        }
    }

}
