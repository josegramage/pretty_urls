
<html>
    <head>
        <title>
            <?php if (!isset($_GET['module'])) {
                echo "Home";
            } else {
                echo $_GET['module'];
            } ?>
        </title>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta charset="UTF-8">
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,500,900' rel='stylesheet' type='text/css'>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>


        <link rel="stylesheet" href="<?php echo CSS_PATH ?>bootstrap.min.css" />
      <noscript> 
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>skel-noscript.css" />
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>style.css">  
        <link rel="stylesheet" href="<?php echo CSS_PATH ?>style-desktop.css" />
       </noscript>  

        <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

        <!--Necesarios Vista-->
   <!--     <script src="view/js/skel.min.js"></script>
        <script src="view/js/skel-panels.min.js"></script>
        <script src="view/js/init.js"></script>   -->

        <script type="text/javascript" src="<?php echo JS_PATH ?>skel.min.js" ></script>
        <script type="text/javascript" src="<?php echo JS_PATH ?>skel-panels.min.js" ></script>
        <script type="text/javascript" src="<?php echo JS_PATH ?>init.js" ></script>

        <!-- BOOTSTRAP SCRIPTS  -->

        <script type="text/javascript" src="<?php echo JS_PLUGINS ?>bootstrap.js"></script>
    <!--   <script src="view/plugins/bootstrap.js"></script>  -->

    </head>