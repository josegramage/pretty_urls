<?php

function loadModel($model_path, $model_name, $function, $arrArgument = '') {
    $model = $model_path . $model_name . '.class.singleton.php';

    if (file_exists($model)) {
        include_once($model);

        $modelClass = $model_name;

        if (!method_exists($modelClass, $function)) {
            throw new Exception;
        }

        $obj = $modelClass::getInstance();

        if (isset($arrArgument)) {
            return $obj->$function($arrArgument);

             //return  call_user_func(array($obj,$function),$arrArgument);
        }
    } else {
        $log = log::getInstance();
        $log->add_log_general("error loadModel general", $_GET['module'], "response " . http_response_code()); //$text, $controller, $function
        $log->add_log_user("error loadModelView general", "", $_GET['module'], "response " . http_response_code()); //$msg, $username = "", $controller, $function
        throw new Exception;
    }
}

function loadView($rutaVista='', $templateName='', $arrPassValue = '') {
    $view_path = $rutaVista . $templateName;
    $arrData = '';

    if (file_exists($view_path)) {
        if (isset($arrPassValue))
            $arrData = $arrPassValue;
        include_once($view_path);
    } else {
        $log = log::getInstance();
        $log->add_log_general("error loadView general", $_GET['module'], "response " . http_response_code()); //$text, $controller, $function
        $log->add_log_user("error loadView general", "", $_GET['module'], "response " . http_response_code()); //$msg, $username = "", $controller, $function

        $result = response_code(http_response_code());
        $arrData = $result;
        require_once VIEW_PATH_INC_ERROR. $result['code'] .'.php';
        die();
    }
}


/*
function loadView($rutaVista = '', $templateName = '', $arrPassValue = '') {
    $view_path = $rutaVista . $templateName;
    $arrData = '';

    if (file_exists($view_path)) {
        if (isset($arrPassValue))
            $arrData = $arrPassValue;
        include_once($view_path);
    } else {
        
        $result = filter_num_int($rutaVista);
        if($result['resultado']) {
            $rutaVista = $result['datos'];
        } else {
            $rutaVista = http_response_code();
        }
        
        $log = log::getInstance();
        $log->add_log_general("error loadView general", $_GET['module'], "response " . $rutaVista); //$text, $controller, $function
        $log->add_log_prod("error loadView general", "", $_GET['module'], "response " . $rutaVista); //$msg, $username = "", $controller, $function

        $result = response_code($rutaVista);
        $arrData = $result;
        require_once VIEW_PATH_INC . 'error.php';
        //die();
    }
}

function loadModel($model_path, $model_name, $function, $arrArgument = '') {
    $model = $model_path . $model_name . '.class.singleton.php';

    if (file_exists($model)) {

        include_once($model);
        $modelClass = $model_name;

        if (!method_exists($modelClass, $function)) {

            throw new Exception();
        }

        $obj = $modelClass::getInstance();

        if (isset($arrArgument)) {
            //return $obj->$function($arrArgument);
            return call_user_func(array($obj, $function), $arrArgument);
        }
    } else {

        throw new Exception();
    }
}
